LOCAL_PATH := $(call my-dir)


ifeq ($(TARGET_ARCH_ABI), armeabi-v7a)
    DIRNAME := armeabi_v7a-linux-android
else
    ifeq ($(TARGET_ARCH_ABI), armeabi)
        DIRNAME := armeabi-linux-android
    else
        $(error "No support for $(TARGET_ARCH_ABI) so far!")
    endif
endif

include $(CLEAR_VARS)
LOCAL_MODULE := boostexception
LOCAL_SRC_FILES := $(DIRNAME)/lib/libboost_exception.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/$(DIRNAME)/include/
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := boostfilesystem
LOCAL_SRC_FILES := $(DIRNAME)/lib/libboost_filesystem.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/$(DIRNAME)/include/
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := boostmonitor
LOCAL_SRC_FILES := $(DIRNAME)/lib/libboost_prg_exec_monitor.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/$(DIRNAME)/include/
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := boostprogram_options
LOCAL_SRC_FILES := $(DIRNAME)/lib/libboost_program_options.a 
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/$(DIRNAME)/include/
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := boostsignals
LOCAL_SRC_FILES := $(DIRNAME)/lib/libboost_signals.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/$(DIRNAME)/include/
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := boostsystem
LOCAL_SRC_FILES := $(DIRNAME)/lib/libboost_system.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/$(DIRNAME)/include/
include $(PREBUILT_STATIC_LIBRARY)

