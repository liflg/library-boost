#!/bin/bash

set -e

select_multiarchname()
{
    echo "Please select the desired architecture:"
    echo "1): i386-linux-gnu"
    echo "2): x86_64-linux-gnu"
    echo "3): armeabi-linux-android"
    echo "4): armeabi_v7a-linux-android"
    read -r SELECTEDOPTION
    if [ x"$SELECTEDOPTION" = x"1" ]; then
        MULTIARCHNAME=i386-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"2" ]; then
        MULTIARCHNAME=x86_64-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"3" ]; then
        MULTIARCHNAME=armeabi-linux-android
    elif [ x"$SELECTEDOPTION" = x"4" ]; then
        MULTIARCHNAME=armeabi_v7a-linux-android
    else
        echo "Invalid option selected!"
        select_multiarchname
    fi
}

linux_build()
{
    if [ x"$BUILDTYPE" = xdebug ]; then
        VARIANT="debug"
    else
        VARIANT="release"
    fi

    if [[ ( -z "$OPTIMIZATION" ) || ( "$OPTIMIZATION" -eq 3 ) ]]; then
        export MYOPTIMIZATION="optimization=speed"
    elif [ "$OPTIMIZATION" -eq 0 ]; then
        export MYOPTIMIZATION="optimization=off"
    else
        echo "Optimization level '$OPTIMIZATION' is not yet implemented!"
        exit 1
    fi

    rm -rf "$PREFIXDIR"

    ( cd source
      ./bootstrap.sh --with-libraries=filesystem,exception,program_options,test --prefix="$PREFIXDIR"
      ./b2 link=shared runtime-link=shared $MYOPTIMIZATION variant=$VARIANT cxxflags="-std=c++0x" install
      install -m0755 -d "$PREFIXDIR"/bin
      install -m0755 bjam "$PREFIXDIR"/bin/)

    # clean up afterwards
    ( cd source
      git clean -df .
      git checkout .)
}

if [ -z "$MULTIARCHNAME" ]; then
    echo "\$MULTIARCHNAME is not set!"
    select_multiarchname
fi

if [ -z "$PREFIXDIR" ]; then
    PREFIXDIR="$PWD/$MULTIARCHNAME"
fi

case "$MULTIARCHNAME" in
i386-linux-gnu)
    linux_build;;
x86_64-linux-gnu)
    linux_build;;
*)
    echo "$MULTIARCHNAME is not (yet) supported by this script."
    exit 1;;
esac

install -m0664 source/LICENSE_1_0.txt "$PREFIXDIR"/lib/LICENSE-boost.txt

rm -rf "$BUILDDIR"

echo "Boost for $MULTIARCHNAME is ready."
