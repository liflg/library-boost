#!/bin/bash
set -e

FILENAME=`readlink -f $0`
ROOTDIR=`dirname $FILENAME`
cd "$ROOTDIR"
export PATH="$ROOTDIR/bjam/bin/":$PATH

setupBjam() {
    git checkout source/tools/build/v2/user-config.jam
    (cd $ROOTDIR/source/tools/build/v2 && ./bootstrap.sh && ./b2 install --build-dir="$ROOTDIR/build_bjam" --prefix=$ROOTDIR/bjam )
}

#Requires one argument: arm or x86
setupToolchain() {
    export TOOLCHAINARCH=$1
    export TOOLCHAINDIR="${ROOTDIR}/android-toolchain-${TOOLCHAINARCH}"
    export TOOLCHAINPLATFORM=android-9

    case $1 in
    arm)
        echo "Building for Android armeabi";;
    x86)
        echo "Building for Android x86";;
    *)
        echo "Unsupported Android toolchain architecture: $1"
        exit 254;; 
    esac

    make-standalone-toolchain.sh \
        --platform=${TOOLCHAINPLATFORM} \
        --install-dir=${TOOLCHAINDIR} \
        --arch=${TOOLCHAINARCH}

    export PATH=${TOOLCHAINDIR}/bin:${PATH}
    export CC=${TOOLCHAINARCH}-linux-androideabi-gcc
    export CXX=${TOOLCHAINARCH}-linux-androideabi-g++
}

buildBoost() {
    ( cd source &&
    ./bootstrap.sh --with-libraries=filesystem,exception,program_options,test,signals --prefix="$ROOTDIR/$1" &&
    bjam -q toolset=$2 link=static threading=multi runtime-link=shared --build-dir="$ROOTDIR/build_$1" install &&
    rm -rf "$ROOTDIR/build_$1" )
}

setupBjam
cp "$ROOTDIR/user-config.jam" "$ROOTDIR/source/tools/build/v2/user-config.jam"
setupToolchain arm
#buildBoost armeabi-linux-android gcc-armeabi
buildBoost armeabi_v7a-linux-android gcc-armeabi7va
